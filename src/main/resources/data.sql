INSERT INTO roles VALUES (1,'ROLE_ADMIN');
INSERT INTO roles VALUES (2,'ROLE_SDCS');
INSERT INTO roles VALUES (3,'ROLE_LD');
INSERT INTO users (`user_id`,`active`,`email`,`last_name`,`name`,`password`,`user_name`) VALUES
(1,1,'admin@admin.com','Lastsom','Namie','$2a$10$8rPjAKuK9ENaFuaKLg9GdOCsjGgK6tjLzntoiT2Fang1H2feD4qLW','admin');
INSERT INTO user_role VALUES (1,1);